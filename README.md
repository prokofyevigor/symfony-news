Test project Symfony 3.2
========================

# News platform

### Install

```composer install```

### Config

```cp app/config/parameters.yml.dist app/config/parameters.yml```

and change DB paramaters

### Migrations

```php bin/console doctrine:migrations:migrate```

### Fixtures

```php bin/console doctrine:fixtures:load```


## Authentificate

email: user@example.com

password: demo

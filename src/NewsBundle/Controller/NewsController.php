<?php

namespace NewsBundle\Controller;

use NewsBundle\Entity\News;
use NewsBundle\Entity\Comment;
use NewsBundle\Form\AddCommentFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller
{

    public function indexAction(Request $request)
    {
        $news = $this->repository()->latest();

        // pagination
        $paginator  = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);

        $pagination = $paginator->paginate($news, $page, 5);

        return $this->render('NewsBundle:News:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Show news action
     * + store comment
     *
     * @param Request $request
     * @param integer $id
     * @param string $slug
     *
     * @return \NewsBundle\Repository\NewsRepository
     */
    public function showAction(Request $request, $id, $slug)
    {
        $entity = $this->repository()->find($id);
        if (is_null($entity) || $slug !== $entity->getSlug()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(AddCommentFormType::class, new Comment(), ['method' => 'POST']);

        // persist comment
        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                // save comment
                $comment = $form->getData();
                $comment->setNews($entity);

                if ($user = $this->getUser()) {
                    $comment->setUser($user);
                }
                // persist
                $this->manager()->persist($comment);
                $this->manager()->flush();

                return $this->redirect($this->generateUrl('news_show', [
                    'id' => $entity->getId(), 'slug' => $entity->getSlug()
                ]));
            }
        }

        return $this->render('NewsBundle:News:show.html.twig', [
            'entity' => $entity,
            'commentForm' => $form->createView(),
        ]);
    }

    /**
     * Get News repository
     *
     * @return \NewsBundle\Repository\NewsRepository
     */
    protected function repository()
    {
        return $this->getDoctrine()->getRepository('NewsBundle:News');
    }

    /**
     * Get doctrine EntityManager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function manager()
    {
        return $this->getDoctrine()->getManager();
    }

}

<?php

namespace NewsBundle\Controller;

use NewsBundle\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommentController extends Controller
{

    public function storeAction(Request $request)
    {

    }

    /**
     * Get Comment repository
     *
     * @return \NewsBundle\Repository\CommentRepository
     */
    protected function repository()
    {
        return $this->getDoctrine()->getRepository('NewsBundle:Comment');
    }

    /**
     * Get doctrine EntityManager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function manager()
    {
        return $this->getDoctrine()->getManager();
    }

}

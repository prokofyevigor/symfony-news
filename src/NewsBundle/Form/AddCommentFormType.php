<?php

namespace NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddCommentFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // email
        $builder->add('username', TextType::class, ['label' => 'Your name', 'attr' => ['class' => 'form-control', 'placeholder' => 'Type your name']]);
        $builder->add('body', TextareaType::class, ['label' => 'Commentary', 'attr' => ['class' => 'form-control noresize', 'cols'=>5, 'placeholder' => 'Type comments']]);

        // submit
        $builder->add('send', SubmitType::class, ['attr' => [
            'class' => 'btn btn-success',
            'style' => 'margin-top: 20px',
        ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NewsBundle\Entity\Comment'
        ));
    }


}

<?php
namespace NewsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NewsBundle\Repository\NewsRepository;
use UserBundle\Entity\User;
use NewsBundle\Entity\News;
use NewsBundle\Entity\Comment;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadNewsData implements FixtureInterface, OrderedFixtureInterface
{

    protected $sentences;

    protected $manager;

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        // get user
        $userEntities = $this->manager->getRepository('UserBundle:User')->findByEmail('user@example.com');        
        $user = $userEntities[0];

        // generate news
        $newsFeed = $this->getNewsFeed();
        foreach ($newsFeed as $feed) {
            $news = new News();
            $slug = $news->slugify($feed['title']);

            if (! $this->manager->getRepository('NewsBundle:News')->findBySlug($slug)) {
                $news->setPublished(true);
                $news->setTitle($feed['title']);
                $news->setPreviewText($feed['preview_text']);
                $news->setPreviewImage($feed['preview_image']);
                $news->setBody($feed['body']);
                if (!is_null($user)) {
                    $news->setUser($user);
                }

                $this->manager->persist($news);

                // gen comments
                $this->makeComments($feed['comments_count'], $news, $user);
            }
        }

        $this->manager->flush();
    }

    /**
     * Load comments to one news
     *
     * @return string
     */
    protected function makeComments($count, News $news, User $user = null)
    {
        for ($index = 0; $index < $count; $index++) {
            $comment = new Comment();
            $comment->setBody($this->randomSentence());
            if (!is_null($user) && rand(0,100) < 70) {
                $comment->setUser($user);
                $comment->setUsername($user->getName());
            }
            else {
                $comment->setUsername($this->randomSentence());
            }

            $news->addComment($comment);

            $this->manager->persist($comment);
        }
    }

    /**
     * Get random username
     *
     * @return string
     */
    protected function randomUsername()
    {
        $names = $this->getPeopleNames();
        $index = rand(0, count($names) - 1);

        return $sentences[$names];
    }

    /**
     * Get array of people names / usernames
     *
     * @return string
     */
    protected function getPeopleNames()
    {
        return [
            'Shalanda Hee',
            'Jon Ardoin',
            'Trula Meyers',
            'Jessika Gregorio',
            'Deon Demartini',
            'Masako Kesselman',
            'Sudie Truman',
            'Alease Ariza',
        ];
    }

    /**
     * Get random commentary from 'ipsum'
     *
     * @return string
     */
    protected function randomSentence()
    {
        $sentences = $this->getSentences();
        $index = rand(0, count($sentences) - 1);

        return $sentences[$index];
    }

    /**
     * Generate many sentences from 'lorem ipsum' data
     *
     * @return array
     */
    protected function getSentences($index = null)
    {
        if (!empty($this->sentences)) {
            return is_null($index) ? $this->sentences : $this->sentences[$index];
        }
        $this->sentences = $this->splitSentences($this->ipsum());

        return is_null($index) ? $this->sentences : $this->sentences[$index];
    }

    /**
     * Split sentences
     *
     * @param string $text
     * @return array
     */
    protected function splitSentences($text)
    {
        return preg_split('/(?<=[.?!;:])\s+/', $text, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Get news feed
     *
     * @return array
     */
    protected function getNewsFeed()
    {
        return [
            [
                'comments_count' => 12,
                'title' => '1GOOGLE WILL START ADDING ASSISTANT TO NOUGAT AND MARSHMALLOW PHONES THIS WEEK',
                'preview_text' => 'Assistant might be the future of voice-controlled intelligence on our Google devices, but Google hasn’t made it easy to get it.',
                'preview_image' => 'http://core0.staticworld.net/images/article/2016/10/pixel_xl_google_assistant_how_can_i_help-100688030-large.jpg',
                'body' => '<p>Assistant might be the future of voice-controlled intelligence on our Google devices, but Google hasn’t made it easy to get it. There are versions of it on Google Home, the new LG watches, and in Allo chats, but if you wanted the full mobile Assistant experience, you had to commit to buying one of the Pixel phones.</p>
<p>That’s all about to change with a coming Play Services update. Right on the heels of LG’s declaration that the G6 would be the first third-party phone to feature Assistant, Google announced that it would be bringing its voice-activated concierge to all phones running Nougat and Marshmallow.</p>
<p>As it is with the Pixel, users will be able to summon Assistant by saying “OK Google” or long-pressing on the home button. While Assistant’s capabilities vary depending on which device or app you’re using, it appears as though the full Pixel version will be used here, letting you tap into Maps and Calendar, receive weather reports, and control smart devices, among numerous other commands.</p>
<p>iBest Bluetooth speakers: We\'ll help you find the best wireless speakers for pairing with your smartphone or tablet—whatever your budget, and whatever music floats your boat.</p>
<p>Google says Assistant will begin rolling out this week to English users in the U.S., followed by Australia, Canada, and the UK, and German speakers in Germany. While Assistant on the Pixel also supports Hindi, Japanese, and Portuguese, Google says it will continue to add more languages throughout the year.</p>'
            ],
            [
                'comments_count' => 4,
                'title' => 'SAMSUNG MULLS IRIS SCANNERS ON SMARTPHONES TO LOG INTO WINDOWS PCS',
                'preview_text' => 'Assistant might be the future of voice-controlled intelligence on our Google devices, but Google hasn’t made it easy to get it.',
                'preview_image' => 'http://core0.staticworld.net/images/article/2017/02/crop-100710410-large.jpg',
                'body' => '<p>Samsung is providing the ability to log into its Windows 10 PCs with Galaxy smartphones for convenience and security. For example, users will be able to bypass Windows Hello and keep retina scan information on a smartphone once that feature is available.</p>
<p>Otherwise, a user now can swipe a finger on a Galaxy smartphone or use pattern authentication to log into Galaxy Book. That\'s a unique feature and independent of Windows Hello. The Galaxy Book doesn\'t have a fingerprint scanner, so the smartphone is needed for that. An NFC connection is established for smartphone-based logins into Windows PCs.</p>
<p>Samsung is working with Microsoft to integrate more advanced authentication features, said Eric McCarty, vice president of mobile product marketing for Samsung Electronics America.</p>
<p>Samsung has some unique biometric authentication technology on its handsets that could be used to log into Windows PCs. The now defunct Galaxy Note 7 had an iris scanner, which could make it to future Galaxy handsets.</p>
<p>In addition to its efforts on authentication, Samsung is trying to figure out ways to better link up its Android handsets, Windows PCs and Tizen OSes. There is a considerable gap between Samsung\'s Windows PCs and devices with Tizen, like smartwatches and TVs. Samsung Flow links the Galaxy Books only to Android handsets, not Tizen devices.</p>
<p>That\'s a big hole in an otherwise strong product lineup, and keeps it a step behind main rival Apple, whose devices link up seamlessly.</p>
<p>Samsung is also looking at ways for wearables to better communicate with its PCs, McCarty said.</p>'
            ],
            [
                'comments_count' => 6,
                'title' => 'Assistant might be the future of voice-controlled intelligence on our Google devices',
                'preview_text' => 'Assistant might be the future of voice-controlled intelligence on our Google devices, but Google hasn’t made it easy to get it.',
                'preview_image' => 'http://core0.staticworld.net/images/article/2017/02/p1210726_1-100710571-large.jpg',
                'body' => '<p>Enterprises and cloud companies will start trying their hands at cellular this year, Nokia President and CEO Rajeev Suri predicts.</p>
<p>“Enhanced reality” and events such as concerts may be where cloud giants first get into mobile services, Suri said at a Nokia event in Barcelona on the eve of Mobile World Congress.</p>
<p>“The first webscale players will enter the wireless access domain with mainstream technologies,” Suri said. Webscale usually refers to operators of big clouds, like Google, Facebook, and Alibaba. Suri didn’t name any names.</p>
<p>Best Bluetooth speakers: We\'ll help you find the best wireless speakers for pairing with your smartphone or tablet—whatever your budget, and whatever music floats your boat.</p>
<p>For enterprises, an emerging technique called network slicing will allow them to virtually run their own private services on mobile operator networks. Meanwhile, systems that bring LTE to unlicensed or shared frequencies, like LAA< (Licensed Assisted Access), will also help open doors to private cellular networks. Nokia is already working with some energy utilities on these kinds of deployments, and at MWC it will join Qualcomm in demonstrating a private LTE network, Suri said.</p>',
            ],
            [
                'comments_count' => 2,
                'title' => 'Intelligence on our Google devices',
                'preview_text' => 'The first webscale players will enter the wireless access domain with mainstream technologies.',
                'preview_image' => 'http://core0.staticworld.net/images/article/2017/02/p1210726_1-100710571-large.jpg',
                'body' => '<p>“The first webscale players will enter the wireless access domain with mainstream technologies,” Suri said. Webscale usually refers to operators of big clouds, like Google, Facebook, and Alibaba. Suri didn’t name any names.</p>
<p>Best Bluetooth speakers: We\'ll help you find the best wireless speakers for pairing with your smartphone or tablet—whatever your budget, and whatever music floats your boat.</p>
<p>For enterprises, an emerging technique called network slicing will allow them to virtually run their own private services on mobile operator networks. Meanwhile, systems that bring LTE to unlicensed or shared frequencies, like LAA< (Licensed Assisted Access), will also help open doors to private cellular networks. Nokia is already working with some energy utilities on these kinds of deployments, and at MWC it will join Qualcomm in demonstrating a private LTE network, Suri said.</p>',
            ],
            [
                'comments_count' => 1,
                'title' => $this->getSentences(0),
                'preview_text' => $this->randomSentence(),
                'preview_image' => null,
                'body' => '<p>' . $this->ipsum() . '</p>',
            ],
            [
                'comments_count' => 0,
                'title' => $this->getSentences(1),
                'preview_text' => $this->randomSentence(),
                'preview_image' => null,
                'body' => '<p>' . $this->ipsum() . '</p>',
            ],
            [
                'comments_count' => 5,
                'title' => $this->getSentences(2),
                'preview_text' => $this->randomSentence(),
                'preview_image' => null,
                'body' => '<p>' . $this->ipsum() . '</p>',
            ],
            [
                'comments_count' => 3,
                'title' => $this->getSentences(3),
                'preview_text' => $this->randomSentence(),
                'preview_image' => null,
                'body' => '<p>' . $this->ipsum() . '</p>',
            ],

        ];
    }

    protected function ipsum()
    {
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.";
    }

}

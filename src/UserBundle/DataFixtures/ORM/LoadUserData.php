<?php
namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\User;
use UserBundle\Entity\Role;

// interfaces
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    private $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->setContainer($container);
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        // create role
        $role = new Role();
        $role->setName('User');
        $role->setRole('ROLE_USER');

        $manager->persist($role);

        // create user
        $user = new User();
        $user->setId(1);
        $user->setName('John Doe');
        $user->setEmail('user@example.com');
        $user->setSalt();

        $password = 'demo';

        $hash = $this->encodePassword($user, $password);
        $user->setPassword($hash);

        $user->getRoles()->add($role);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * Encode password
     *
     * @param UserBundle\Entity\User $user
     * @param $string $plainPassword
     *
     * @return string
     */
    private function encodePassword(User $user, $plainPassword)
    {
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

}

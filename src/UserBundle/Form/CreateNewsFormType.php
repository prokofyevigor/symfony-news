<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateNewsFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('published', CheckboxType::class, ['label' => 'Publicate', 'attr' => ['checked' => 'checked', 'style' => 'margin:15px']]);
        $builder->add('title', TextType::class, ['label' => 'Title', 'attr' => ['class' => 'form-control']]);
        $builder->add('preview_text', TextType::class, ['label' => 'Preview text', 'attr' => ['class' => 'form-control']]);
        $builder->add('preview_image', TextType::class, ['label' => 'Preview image (url)', 'attr' => ['class' => 'form-control']]);

        // news body
        $builder->add('body', TextareaType::class, ['label' => 'News body', 'attr' => [
            'class' => 'form-control',
            'style' => 'height: 215px'
        ]]);

        // submit
        $builder->add('submit', SubmitType::class, ['label' => 'Create / Edit', 'attr' => [
            'class' => 'btn btn-success',
            'style' => 'margin:20px 0',
        ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NewsBundle\Entity\News',
        ));
    }

}

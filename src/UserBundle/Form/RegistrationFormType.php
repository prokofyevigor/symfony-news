<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // fields
        $builder->add('name', TextType::class, ['label' => 'Your Name', 'attr' => ['class' => 'form-control']]);
        $builder->add('email', EmailType::class, ['label' => 'Email', 'attr' => ['class' => 'form-control']]);

        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'required' => true,
            'options' => ['attr' => [
                'class' => 'form-control'
            ]],
            'first_options'  => ['label' => 'Password'],
            'second_options' => ['label' => 'Confirm Password'],
            'attr' => ['class' => 'form-control']
        ]);

        $builder->add('signup', SubmitType::class, ['label' => 'Sign Up', 'attr' => [
            'class' => 'btn btn-success',
            'style' => 'margin-top:20px',
        ]]);
    }

    public function getName()
    {
        return 'registration';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'UserBundle\Entity\User',
        ]);
    }


}

<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // email
        $builder->add('_username', EmailType::class, ['label' => 'Email', 'attr' => ['class' => 'form-control']]);
        $builder->add('_password', PasswordType::class, ['attr' => ['class' => 'form-control']]);

        // submit
        $builder->add('signin', SubmitType::class, ['label' => 'Sign in', 'attr' => [
            'class' => 'btn btn-success',
            'style' => 'margin-top:20px',
        ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

}

<?php

namespace UserBundle\Controller;

use UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use UserBundle\Form\LoginFormType;
use UserBundle\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AuthController extends Controller
{

    protected $firewall = 'main';

    /**
     * Sign in user (login form)
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return User
     */
    public function loginAction(Request $request)
    {
        $authUtils = $this->get('security.authentication_utils');

        return $this->render('UserBundle:Auth:login.html.twig', [
            //'form' => $form->createView(),
            'last_username' => $authUtils->getLastUsername(),
            'error'         => $authUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * Register user action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return User
     */
    public function registerAction(Request $request)
    {
        $registration = new User();
        $form = $this->createForm(RegistrationFormType::class, $registration, [
            'action' => $this->generateUrl('user_register'),
            'method' => 'POST',
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                // create user
                $user = $this->createUser($form);

                // authentificate user
                $this->logUser($request, $user);
                //dump($this->getUser());die();

                return $this->redirect($this->generateUrl('user_news'));
            }
        }

        return $this->render('UserBundle:Auth:register.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * Login check action (not use)
     */
    public function loginCheckAction()
    {
        // do not use
    }

    /**
     * Logout user
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return User
     */
    public function logoutAction()
    {
        $this->container->get('security.token_storage')->setToken(null);
        $this->container->get('session')->invalidate();

        return $this->redirect($this->generateUrl('news_index'));
    }

    /**
     * Create User from form request
     *
     * @param \Symfony\Component\Form\Form $form
     *
     * @return User
     */
    protected function createUser(\Symfony\Component\Form\Form $form)
    {
        $user = $form->getData();

        // encode password
        $encoder = $this->container->get('security.password_encoder');
        $password = $user->getPassword();
        $hash = $encoder->encodePassword($user, $password);
        $user->setPassword($hash);

        // set role
        $roleUser = $this->repository('UserBundle:Role')->getBySlug('ROLE_USER');
        $user->setRole($roleUser);

        $this->manager()->persist($user);
        $this->manager()->flush();

        return $user;
    }

    /**
     * Authentificate user
     *
     * @param UserIntarface $user
     *
     * @return void
     */
    protected function logUser(Request $request, UserInterface $user)
    {
        $token = new UsernamePasswordToken($user, 'demo', 'main', ['ROLE_USER']);
        $this->container->get('security.token_storage')->setToken($token);
    }

    /**
     * Get repository
     *
     * @param string $name
     *
     * @return \NewsBundle\Repository\NewsRepository
     */
    protected function repository($name = 'UserBundle:User')
    {
        return $this->getDoctrine()->getRepository($name);
    }

    /**
     * Get doctrine EntityManager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function manager()
    {
        return $this->getDoctrine()->getManager();
    }

}

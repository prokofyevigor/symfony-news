<?php

namespace UserBundle\Controller;

use NewsBundle\Entity\News;
use UserBundle\Form\CreateNewsFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserNewsController extends Controller
{

    /**
     * List of news by this user
     *
     * @param integer $id
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // auth check
        if ($this->notAuth()) {
            throw $this->createNotFoundException();
        }

        $news = $this->getUser()->getNews();

        // pagination
        $paginator  = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);

        $pagination = $paginator->paginate($news, $page, 10);

        return $this->render('UserBundle:UserNews:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Create news
     *
     * @param integer $id
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CreateNewsFormType::class, new News(), ['method' => 'POST']);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                // store
                $news = $form->getData();
                $news->setUser($this->getUser());

                // persist
                $this->manager()->persist($news);
                $this->manager()->flush();

                return $this->redirect($this->generateUrl('user_news'));
            }
        }

        return $this->render('UserBundle:UserNews:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edit news
     *
     * @param integer $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id)
    {
        $news = $this->repository()->find($id);

        // access control
        if (false === $this->accessControl($news)) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(CreateNewsFormType::class, $news, ['method' => 'POST']);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                // store
                $news = $form->getData();
                $news->setUser($this->getUser());

                // persist
                $this->manager()->persist($news);
                $this->manager()->flush();

                return $this->redirect($this->generateUrl('user_news'));
            }
        }

        // access control
        if (is_null($news) || false === $this->accessControl($news)) {
            throw $this->createNotFoundException();
        }

        return $this->render('UserBundle:UserNews:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete news
     *
     * @param integer $id
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $news = $this->repository()->find($id);

        // access control
        if (false === $this->accessControl($news)) {
            throw $this->createNotFoundException();
        }

        // delete
        $this->manager()->remove($news);
        $this->manager()->flush();

        // redirect
        return $this->redirect($this->generateUrl('user_news'));
    }

    /**
     * Check access
     *
     * @param NewsBundle\Entity\News $news
     *
     * @return Response
     */
    protected function accessControl(News $news)
    {
        return !is_null($news)
            && $this->getUser()
            && $news->getUser()->getId() == $this->getUser()->getId();
    }

    /**
     * Check auth
     *
     * @return Response
     */
    protected function notAuth()
    {
        return is_null($this->getUser());
    }

    /**
     * Get repository
     *
     * @param string $name
     *
     * @return \NewsBundle\Repository\NewsRepository
     */
    protected function repository($name = 'NewsBundle:News')
    {
        return $this->getDoctrine()->getRepository($name);
    }

    /**
     * Get doctrine EntityManager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function manager()
    {
        return $this->getDoctrine()->getManager();
    }

}
